﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    private GameManager m_manager;
    public GameManager.Symbols Symbol { get; private set; }
    private Transform m_child;
    private Collider m_collider;

    //
    private void Start()
    {
        //Get the first child of this object and set m_child to it
        m_child = this.transform.GetChild( 0 );
        //Get the collider on the tile
        m_collider = GetComponent<Collider>();
    }

    //When the tile is clicked, set the GameManager's selected tile to this object
    private void OnMouseDown()
    {
        m_manager.TileSelected( this );
    }

    //Set up the tile's symbol based on the game manager
    public void Initialize( GameManager manager, GameManager.Symbols symbol )
    {
        m_manager = manager;
        Symbol = symbol;
    }

    //When clicked
    public void Reveal()
    {
        //Make sure this is the only coroutine running
        StopAllCoroutines();
        //Spin the card around to reveal the symbol
        StartCoroutine( Spin( 180, 0.8f ) );
        //Disable the collider so it can't be clicked
        m_collider.enabled = false;
    }

    //If incorrect match
    public void Hide()
    {
        StopAllCoroutines();
        //Spin the card back around to hide the symbol
        StartCoroutine( Spin( 0, 0.8f ) );
        //Re-enable collider so it's clickable
        m_collider.enabled = true;
    }

    //The animation that spins the card around
    private IEnumerator Spin( float target, float time )
    {
        float timer = 0;
        //Starting rotation is upside down
        float startingRotation = m_child.eulerAngles.y;

        Vector3 euler = m_child.eulerAngles;

        //in 'time' amount of time, rotation card 
        //from it's initial rotation (startingRotation) to its end rotation
        //(startingRotation + target)
        while ( timer < time )
        {
            timer += Time.deltaTime;
            euler.y = Mathf.LerpAngle( startingRotation, target, timer / time );
            m_child.eulerAngles = euler;
            yield return null;
        }
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
