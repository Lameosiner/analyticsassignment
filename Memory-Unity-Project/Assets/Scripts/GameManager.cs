﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    int correctMatches;
    int incorrectMatches;

    public static int totalCardsFlipped;


    public List<Symbols> FrequentlyFoundSymbols;
    public List<int> TopSymbols;

    Symbols topSymbol;

    int LevelsCompleted;

    public int bombsFound, bonesFound, candlesFound, commandsFound, disksFound, dotsFound, dropsFound, facesFound;
    public int flagsFound, handsFound, largeDiamondsFound, smallDiamondsFound, squaresFound, sunsFound, wavesFound, wheelsFound;

    public int[] LifetimeSymbolsFound;

    //Allows you to pick more than one value
    [System.Flags]
    public enum Symbols
    {
        //Each value is bit-shifted to the left by a certain number of positions
        //Representative of the positions of the 1s and 0s in its binary form
        //IE;
        Waves =             1 << 0, //001 = 1
        Dot =               1 << 1, //010 = 2
        Square =            1 << 2, //011 = 3
        LargeDiamond =      1 << 3, //...and so on and so forth...
        SmallDiamonds =     1 << 4,
        Command =           1 << 5,
        Bomb =              1 << 6,
        Sun =               1 << 7,
        Bones =             1 << 8,
        Drop =              1 << 9,
        Face =              1 << 10,
        Hand =              1 << 11,
        Flag =              1 << 12,
        Disk =              1 << 13,
        Candle =            1 << 14,
        Wheel =             1 << 15
    }



    //Using a Struct
    [System.Serializable]
    public struct LevelDescription
    { 
        //Set rows and columns of each level in the inspector
        //Select multiple symbols to use in the level
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    //Tile asset and spacing for level generation
    public GameObject TilePrefab;
    public float TileSpacing;

    //An array of all the levels in the inspector
    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;

    private void Start()
    {
        //Static int for level number for persistent data
        LoadLevel( CurrentLevelNumber );
        LifetimeSymbolsFound = new int[16];
    }

    //
    private void LoadLevel( int levelNumber )
    {
        //Go to the right level in the array and acquire the correct symbols for generation
        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols( level );

        //Grab info for rows
        for ( int rowIndex = 0; rowIndex < level.Rows; ++rowIndex )
        {
            //Place tiles with spacing
            float yPosition = rowIndex * ( 1 + TileSpacing );

            //Grab same info for columns
            for ( int colIndex = 0; colIndex < level.Columns; ++colIndex )
            {
                float xPosition = colIndex * ( 1 + TileSpacing );

                //With the previously set x and y positions, Instantiate a tile at that position before looping incrementally
                GameObject tileObject = Instantiate( TilePrefab, new Vector3( xPosition, yPosition, 0 ), Quaternion.identity, this.transform );
                //Give each tile a random symbol from the selected few
                int symbolIndex = UnityEngine.Random.Range( 0, symbols.Count );
                //Display the symbol on the tile
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
                tileObject.GetComponent<Tile>().Initialize( this, symbols[symbolIndex] );
                symbols.RemoveAt( symbolIndex );
            }
        }

        //Set the appropriate camera settings for the level that's being loaded
        SetupCamera( level );
    }

    //Get the symbols specific to the level 
    private List<Symbols> GetRequiredSymbols( LevelDescription level )
    {
        //Create a list for the symbols
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        
        {
            Array allSymbols = Enum.GetValues( typeof( Symbols ) );

            //At start, set remaining cards to total # of cards
            m_cardsRemaining = cardTotal;
            
            //If the cardTotal is not an even number, throw error
            if ( cardTotal % 2 > 0 )
            {
                new ArgumentException( "There must be an even number of cards" );
            }

            //Check each symbol against the full list
            //If the symbol is one of the symbols to be used in the level, add it to the list, otherwise move on
            foreach ( Symbols symbol in allSymbols )
            {
                if ( ( level.UsedSymbols & symbol ) > 0 )
                {
                    symbols.Add( symbol );
                }
            }
        }

        //Debugging in case something is missing
        {
            if ( symbols.Count == 0 )
            {
                new ArgumentException( "The level has no symbols set" );
            }
            if ( symbols.Count > cardTotal / 2 )
            {
                new ArgumentException( "There are too many symbols for the number of cards." );
            }
        }

        //
        {
            //Divide the total number of cards by 2
            int repeatCount = ( cardTotal / 2 ) - symbols.Count;

            //If there are enough cards for there to be any repetition of symbols
            if ( repeatCount > 0 )
            {
                //Create a new list to store the duplicates
                List<Symbols> symbolsCopy = new List<Symbols>( symbols );
                List<Symbols> duplicateSymbols = new List<Symbols>();

                //Randomly go through the list for 'repeatIndex' # of times
                //Duplicate symbols and add them to the list to be instantiated.
                for ( int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex )
                {
                    int randomIndex = UnityEngine.Random.Range( 0, symbolsCopy.Count );
                    duplicateSymbols.Add( symbolsCopy[randomIndex] );
                    symbolsCopy.RemoveAt( randomIndex );
                    if ( symbolsCopy.Count == 0 )
                    {
                        symbolsCopy.AddRange( symbols );
                    }
                }
                symbols.AddRange( duplicateSymbols );
            }
        }

        symbols.AddRange( symbols );

        return symbols;
    }

    //Calculate the offset from each symbol
    //If there is no symbol, set offset to 0, 0
    private Vector2 GetOffsetFromSymbol( Symbols symbol )
    {
        Array symbols = Enum.GetValues( typeof( Symbols ) );
        for ( int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex )
        {
            if ( ( Symbols )symbols.GetValue( symbolIndex ) == symbol )
            {
                return new Vector2( symbolIndex % 4, symbolIndex / 4 ) / 4f;
            }
        }
        Debug.Log( "Failed to find symbol" );
        return Vector2.zero;
    }

    //
    private void SetupCamera( LevelDescription level )
    {
        //Camera automatically scales and repositions so the tiles are always in frame
        Camera.main.orthographicSize = ( level.Rows + ( level.Rows + 1 ) * TileSpacing ) / 2;
        Camera.main.transform.position = new Vector3( ( level.Columns * ( 1 + TileSpacing ) ) / 2, ( level.Rows * ( 1 + TileSpacing ) ) / 2, -10 );
    }

    //When you click on a tile
    public void TileSelected( Tile tile )
    {
        //If a tile has been clicked, set m_tileOne as the first tile and flip it over
        if ( m_tileOne == null )
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        //If two tiles have been clicked, set m_tileTwo as the second tile
        //Flip it over
        else if ( m_tileTwo == null )
        {
            m_tileTwo = tile;
            m_tileTwo.Reveal();

            //If the symbol on both cards is the same, match = true
            if ( m_tileOne.Symbol == m_tileTwo.Symbol )
            {

                if (m_tileOne.Symbol == Symbols.Bomb)
                {
                    bombsFound += 1;
                    LifetimeSymbolsFound[0] = bombsFound;
                }
                if (m_tileOne.Symbol == Symbols.Bones)
                {
                    bonesFound += 1;
                    LifetimeSymbolsFound[1] = bonesFound;

                }
                if (m_tileOne.Symbol == Symbols.Candle)
                {
                    candlesFound += 1;
                    LifetimeSymbolsFound[2] = candlesFound;

                }
                if (m_tileOne.Symbol == Symbols.Command)
                {
                    commandsFound += 1;
                    LifetimeSymbolsFound[3] = commandsFound;

                }
                if (m_tileOne.Symbol == Symbols.Disk)
                {
                    disksFound += 1;
                    LifetimeSymbolsFound[4] = disksFound;

                }
                if (m_tileOne.Symbol == Symbols.Dot)
                {
                    dotsFound += 1;
                    LifetimeSymbolsFound[5] = dotsFound;

                }
                if (m_tileOne.Symbol == Symbols.Drop)
                {
                    dropsFound += 1;
                    LifetimeSymbolsFound[6] = dropsFound;

                }
                if (m_tileOne.Symbol == Symbols.Face)
                {
                    facesFound += 1;
                    LifetimeSymbolsFound[7] = facesFound;

                }
                if (m_tileOne.Symbol == Symbols.Flag)
                {
                    flagsFound += 1;
                    LifetimeSymbolsFound[8] = flagsFound;

                }
                if (m_tileOne.Symbol == Symbols.Hand)
                {
                    handsFound += 1;
                    LifetimeSymbolsFound[9] = handsFound;
                    
                }
                if (m_tileOne.Symbol == Symbols.LargeDiamond)
                {
                    largeDiamondsFound += 1;
                    LifetimeSymbolsFound[10] = largeDiamondsFound;

                }
                if (m_tileOne.Symbol == Symbols.SmallDiamonds)
                {
                    smallDiamondsFound += 1;
                    LifetimeSymbolsFound[11] = smallDiamondsFound;

                }
                if (m_tileOne.Symbol == Symbols.Square)
                {
                    squaresFound += 1;
                    LifetimeSymbolsFound[12] = squaresFound;

                }
                if (m_tileOne.Symbol == Symbols.Sun)
                {
                    sunsFound += 1;
                    LifetimeSymbolsFound[13] = sunsFound;

                }
                if (m_tileOne.Symbol == Symbols.Waves)
                {
                    wavesFound += 1;
                    LifetimeSymbolsFound[14] = wavesFound;
                
                }
                if (m_tileOne.Symbol == Symbols.Wheel)
                {
                    wheelsFound += 1;
                    LifetimeSymbolsFound[15] = wheelsFound;

                }

                int Maxval = Mathf.Max(LifetimeSymbolsFound);
                //print(Maxval);



                if(Maxval == LifetimeSymbolsFound[5])
                {
                    topSymbol = Symbols.Dot;
                }

                //print(topSymbol);

                Analytics.CustomEvent("FoundSymbol", new Dictionary<string, object>
             {
                 { "MostFoundSymbol", topSymbol },
             });

                // FrequentlyFoundSymbols.Add(m_tileOne.Symbol);

                StartCoroutine( WaitForHide( true, 1f ) );
            }

            //otherwise flip them back over
            else
            {
                StartCoroutine( WaitForHide( false, 1f ) );
            }
        }
    }

    //Upon completing the current level
    private void LevelComplete()
    {
      
   
        //Increment current level number to progress
        ++CurrentLevelNumber;

        LevelsCompleted = CurrentLevelNumber;

        Analytics.CustomEvent("FinishedLevel", new Dictionary<string, object>
             {
                 { "LevelsCompleted", LevelsCompleted },
             });

        //If you reach the final level, game over
        if ( CurrentLevelNumber > Levels.Length - 1 )
        {

            Debug.Log( "GameOver" );
        }
        else
        {


            StartCoroutine(EndLevel());

        }
    }

    //
    private IEnumerator WaitForHide( bool match, float time )
    {
        //short timer before cards flip around
        float timer = 0;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            if ( timer >= time )
            {
                break;
            }
            yield return null;
        }
        //if the cards match, destroy them both and reduce the remaining card # by 2
        if ( match )
        {
            Destroy( m_tileOne.gameObject );
            Destroy( m_tileTwo.gameObject );
            m_cardsRemaining -= 2;

            //Every time you correctly match a pair, increment this number
            correctMatches += 1;

            Analytics.CustomEvent("Matched", new Dictionary<string, object>
             {
                 { "CorrectMatches", correctMatches },
             });          


        }
        //otherwise flip them back over
        else
        {
            m_tileOne.Hide();
            m_tileTwo.Hide();

            //Every time you pick 2 cards that don't match
            incorrectMatches += 1;

            Analytics.CustomEvent("FailedMatch", new Dictionary<string, object>
             {
                 { "IncorrectMatches", incorrectMatches },
             });

        }
        //set current selected tiles #1 and #2 to null (remove selection)
        m_tileOne = null;
        m_tileTwo = null;

        //once all cards are matched, level is complete
        if ( m_cardsRemaining == 0 )
        {
            LevelComplete();
        }
    }

    IEnumerator EndLevel()
    {
        totalCardsFlipped = correctMatches + incorrectMatches;

        Analytics.CustomEvent("TallyCards", new Dictionary<string, object>
             {
                 { "TotalCardsFlipped", totalCardsFlipped },
             });

        yield return new WaitForSeconds(1f);



        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }



}
